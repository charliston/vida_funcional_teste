var VidaFuncional = window.VidaFuncional = Ember.Application.create({
    // Basic logging, e.g. "Transitioned into 'post'"
    LOG_TRANSITIONS: true,
    // Extremely detailed logging, highlighting every internal step made while transitioning into a route
    LOG_TRANSITIONS_INTERNAL: true,
    // Log generated controller
    LOG_ACTIVE_GENERATION: true,
    // Log view lookups
    LOG_VIEW_LOOKUPS: true
});

/* Order and include as you please. */
require('scripts/controllers/*');
require('scripts/store');
require('scripts/models/*');
require('scripts/routes/*');
require('scripts/components/*');
require('scripts/views/*');
require('scripts/router');

var inflector = Ember.Inflector.inflector;
inflector.irregular('servidor', 'servidores');
