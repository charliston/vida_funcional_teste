VidaFuncional.Router.map(function () {
  // Add your routes here
    this.route('login');
    this.resource('servidores', function(){
        this.resource('servidor', { path:'ver/:servidor_id' });
    });
});
