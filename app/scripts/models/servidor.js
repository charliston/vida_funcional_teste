VidaFuncional.Servidor = DS.Model.extend({
    servidor: DS.attr('string'),
    matricula: DS.attr('string'),
    rg: DS.attr('string'),
    cpf: DS.attr('string'),
    mae: DS.attr('string'),
    pai: DS.attr('string'),
    data_nascimento: DS.attr('string'),
    estado_civil: DS.attr('string'),
    situacao: DS.attr('string'),
    eventos: DS.hasMany('evento', { async: true })
});
