VidaFuncional.Evento = DS.Model.extend({
    evento: DS.attr('string'),
    numero_diario: DS.attr('string'),
    data_diario: DS.attr('string'),
    pagina: DS.attr('number'),
    data_efeito: DS.attr('string'),
    data_fim: DS.attr('string'),
    tipo_ato: DS.attr('string'),
    numero_ato: DS.attr('string'),
    data_ato: DS.attr('string'),
    vinculo_funcional: DS.attr('string'),
    ver_evento: DS.attr('string'),
    tempo_vinculo: DS.attr('string'),
    servidor: DS.belongsTo('servidor')
});
