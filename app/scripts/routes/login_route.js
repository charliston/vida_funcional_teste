/*
VidaFuncional.TemplateARoute = Ember.Route.extend({
    renderTemplate: function(controller,model) {
        this.send('changeLayout','layoutA');
        this._super(controller,model);
    }
});
*/

VidaFuncional.LoginRoute = Ember.Route.extend({
    renderTemplate: function(controller,model) {
        this.send('changeLayout','_login');
        this._super(controller,model);
        controller.reset();
    }
});

