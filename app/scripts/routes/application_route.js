VidaFuncional.ApplicationRoute = Ember.Route.extend({
    isLoading: false,
    actions: {
        loading: function() {
            if(this.get('isLoading')){
                return;
            }
            this.set('isLoading', true);
            NProgress.start();

            return this.router.one('didTransition', (function(_this) {
                return function() {
                    _this.set('isLoading', false);
                    NProgress.done();
                };
            })(this));
        },
        changeLayout: function(templateName) {
            this.render(templateName);
        }
    }
});
