VidaFuncional.IndexRoute = VidaFuncional.AuthenticatedRoute.extend({
    model: function() {
        return this.getJSONWithToken('/articles.json');
    },
    renderTemplate: function(controller,model) {
        this.send('changeLayout','application');
        this._super(controller,model);
    }
});
