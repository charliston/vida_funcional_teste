VidaFuncional.ApplicationController = Ember.Controller.extend({
    copyright: function(){
        return (new Date()).getFullYear() + ' © DocCenter.';
    }.property()
});
