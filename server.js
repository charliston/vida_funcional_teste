var express = require('express'),
    bodyParser = require('body-parser'),
    app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var SERVIDORES = {
    'servidores':[
        {
            id: 1,
            servidor: 'CARLOS MARIA DE ARRUDA',
            matricula: '20187',
            rg: '1175319 SSP/MT',
            cpf: '173.351.671-91',
            mae: 'HILDA BENEDITA DE ARRUDA',
            pai: 'MANOEL BENEDITO DE ARRUDA',
            data_nascimento: '15/08/1958',
            estado_civil: 'COMPANHEIRO(A)',
            situacao: 'Em Processo de Revisão',
            eventos: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
        }, {
            id: 2,
            servidor: 'BENEDITA MARIA VASCO REIS',
            matricula: '22274',
            rg: '543971 SSP/MT',
            cpf: '378.330.491-15',
            mae: 'AURELIANA MARIA VASCO REIS',
            pai: 'RAIMUNDO ALVES REIS',
            data_nascimento: '28/10/1966',
            estado_civil: 'SOLTEIRO(A)',
            situacao: 'Aguardando',
            eventos: []
        }
    ]
};

var EVENTOS = {
    'eventos': [
        {
            id: 1,
            servidor_id: 1,
            evento: 'admissao',
            numero_diario: '19158',
            data_diario: '2 de outubro de 1984',
            pagina: 8,
            data_efeito: '2 de outubro de 1984',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '4442/84',
            data_ato: '6 de setembro de 1984',
            vinculo_funcional: '',
            ver_evento: '',
            tempo_vinculo: 'tempo anterior'
        },


        {
            id: 2,
            servidor_id: 1,
            evento: 'nomeacao',
            numero_diario: '19311',
            data_diario: '24 de maio de 1985',
            pagina: 9,
            data_efeito: '24 de maio de 1985',
            data_fim: '',
            tipo_ato: 'decreto',
            numero_ato: '1.351',
            data_ato: '24 de maio de 1985',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 3,
            servidor_id: 1,
            evento: 'licenca premio',
            numero_diario: '24375',
            data_diario: '20 de junho de 2006',
            pagina: 9,
            data_efeito: '24 de maio de 2000',
            data_fim: '23 de maio de 2005',
            tipo_ato: 'portaria ',
            numero_ato: '03/sad/o0192/20o6',
            data_ato: '20 de junho de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 4,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '23270 sup.',
            data_diario: '10 de dezembro de 2001',
            pagina: 29,
            data_efeito: '1 de outubro de 2001',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/seduc/00001/2001',
            data_ato: '10 de dezembro de 2001',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 5,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '23970',
            data_diario: '19 de outubro de 2004',
            pagina: 23,
            data_efeito: '1 de outubro de 2004',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: 'ü3/seduc/00419/2004',
            data_ato: '19 de outubro de 2004',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 6,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24170 sup.',
            data_diario: '12 de agosto de 2005',
            pagina: 2,
            data_efeito: '14 de fevereiro de 2005',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/seduc/00447/2005',
            data_ato: '12 de agosto de 2005',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 7,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24502',
            data_diario: '29 de dezembro de 2006',
            pagina: 34,
            data_efeito: '1 de janeiro de 2006',
            data_fim: '31 de janeiro de 2006',
            tipo_ato: 'portaria',
            numero_ato: '446/2006/gab/sejusp',
            data_ato: '19 de dezembro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 8,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24272',
            data_diario: '16 de janeiro de 2006',
            pagina: 12,
            data_efeito: '1 de abril de 2006',
            data_fim: '1 de maio de 2006',
            tipo_ato: 'portaria',
            numero_ato: '08/2005/gab/sejusp',
            data_ato: '13 de janeiro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 9,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24373',
            data_diario: '14 de junho de 2006',
            pagina: 8,
            data_efeito: '1 de abril de 2006',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00155/2006',
            data_ato: '14 de junho de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 10,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24465',
            data_diario: '1 de novembro de 2006',
            pagina: 25,
            data_efeito: '1 de novembro de 2006',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00330/2006',
            data_ato: '1 de novembro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 11,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '24697',
            data_diario: '15 de outubro de 2007',
            pagina: 18,
            data_efeito: '1 de outubro de 2007',
            data_fim: '',
            tipo_ato: 'ato administrativo',
            numero_ato: '1513/2007',
            data_ato: '15 de outubro de 2007',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 12,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24728',
            data_diario: '4 de dezembro de 2007',
            pagina: 16,
            data_efeito: '1 de janeiro de 2008',
            data_fim: '31 de janeiro de 2008',
            tipo_ato: 'portaria',
            numero_ato: '269/2007/gab/sejusp',
            data_ato: '30 de novembro de 2007',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 13,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24774',
            data_diario: '14 de fevereiro de 2008',
            pagina: 14,
            data_efeito: '11 de fevereiro de 2008',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00098/2008',
            data_ato: '14 de fevereiro de 2008',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 14,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24775',
            data_diario: '15 de fevereiro de 2008',
            pagina: 16,
            data_efeito: '13 de fevereiro de 2008',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00102/2008',
            data_ato: '15 de fevereiro de 2008',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 15,
            servidor_id: 1,
            evento: 'concurso',
            numero_diario: '19028',
            data_diario: '27 de mar?o de 1984',
            pagina: 38,
            data_efeito: '',
            data_fim: '',
            tipo_ato: '-',
            numero_ato: '05/84/sad',
            data_ato: '21 de mar?o de 1984',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        }
        ,
        {
            id: 16,
            servidor_id: 1,
            evento: 'admissao',
            numero_diario: '19158',
            data_diario: '2 de outubro de 1984',
            pagina: 8,
            data_efeito: '2 de outubro de 1984',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '4442/84',
            data_ato: '6 de setembro de 1984',
            vinculo_funcional: '',
            ver_evento: '',
            tempo_vinculo: 'tempo anterior'
        },


        {
            id: 17,
            servidor_id: 1,
            evento: 'nomeacao',
            numero_diario: '19311',
            data_diario: '24 de maio de 1985',
            pagina: 9,
            data_efeito: '24 de maio de 1985',
            data_fim: '',
            tipo_ato: 'decreto',
            numero_ato: '1.351',
            data_ato: '24 de maio de 1985',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 18,
            servidor_id: 1,
            evento: 'licenca premio',
            numero_diario: '24375',
            data_diario: '20 de junho de 2006',
            pagina: 9,
            data_efeito: '24 de maio de 2000',
            data_fim: '23 de maio de 2005',
            tipo_ato: 'portaria ',
            numero_ato: '03/sad/o0192/20o6',
            data_ato: '20 de junho de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 19,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '23270 sup.',
            data_diario: '10 de dezembro de 2001',
            pagina: 29,
            data_efeito: '1 de outubro de 2001',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/seduc/00001/2001',
            data_ato: '10 de dezembro de 2001',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 20,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '23970',
            data_diario: '19 de outubro de 2004',
            pagina: 23,
            data_efeito: '1 de outubro de 2004',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: 'ü3/seduc/00419/2004',
            data_ato: '19 de outubro de 2004',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 21,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24170 sup.',
            data_diario: '12 de agosto de 2005',
            pagina: 2,
            data_efeito: '14 de fevereiro de 2005',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/seduc/00447/2005',
            data_ato: '12 de agosto de 2005',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 22,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24502',
            data_diario: '29 de dezembro de 2006',
            pagina: 34,
            data_efeito: '1 de janeiro de 2006',
            data_fim: '31 de janeiro de 2006',
            tipo_ato: 'portaria',
            numero_ato: '446/2006/gab/sejusp',
            data_ato: '19 de dezembro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 23,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24272',
            data_diario: '16 de janeiro de 2006',
            pagina: 12,
            data_efeito: '1 de abril de 2006',
            data_fim: '1 de maio de 2006',
            tipo_ato: 'portaria',
            numero_ato: '08/2005/gab/sejusp',
            data_ato: '13 de janeiro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 24,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24373',
            data_diario: '14 de junho de 2006',
            pagina: 8,
            data_efeito: '1 de abril de 2006',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00155/2006',
            data_ato: '14 de junho de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 25,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24465',
            data_diario: '1 de novembro de 2006',
            pagina: 25,
            data_efeito: '1 de novembro de 2006',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00330/2006',
            data_ato: '1 de novembro de 2006',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 26,
            servidor_id: 1,
            evento: 'progressao',
            numero_diario: '24697',
            data_diario: '15 de outubro de 2007',
            pagina: 18,
            data_efeito: '1 de outubro de 2007',
            data_fim: '',
            tipo_ato: 'ato administrativo',
            numero_ato: '1513/2007',
            data_ato: '15 de outubro de 2007',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 27,
            servidor_id: 1,
            evento: 'escala ferias',
            numero_diario: '24728',
            data_diario: '4 de dezembro de 2007',
            pagina: 16,
            data_efeito: '1 de janeiro de 2008',
            data_fim: '31 de janeiro de 2008',
            tipo_ato: 'portaria',
            numero_ato: '269/2007/gab/sejusp',
            data_ato: '30 de novembro de 2007',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 28,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24774',
            data_diario: '14 de fevereiro de 2008',
            pagina: 14,
            data_efeito: '11 de fevereiro de 2008',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00098/2008',
            data_ato: '14 de fevereiro de 2008',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 29,
            servidor_id: 1,
            evento: 'remocao',
            numero_diario: '24775',
            data_diario: '15 de fevereiro de 2008',
            pagina: 16,
            data_efeito: '13 de fevereiro de 2008',
            data_fim: '',
            tipo_ato: 'portaria',
            numero_ato: '03/sejus/00102/2008',
            data_ato: '15 de fevereiro de 2008',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        },

        {
            id: 30,
            servidor_id: 1,
            evento: 'concurso',
            numero_diario: '19028',
            data_diario: '27 de mar?o de 1984',
            pagina: 38,
            data_efeito: '',
            data_fim: '',
            tipo_ato: '-',
            numero_ato: '05/84/sad',
            data_ato: '21 de mar?o de 1984',
            vinculo_funcional: '',
            ver_evento: 'ver',
            tempo_vinculo: 'tempo anterior'
        }

    ]
};

var ARTICLES = [
    {
        id: 1,
        title: 'How to write a JavaScript Framework',
        author: 'Tomhuda Katzdale',
        body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        id: 2,
        title: 'Chronicles of an Embereño',
        author: 'Alerik Bryneer',
        body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        id: 3,
        title: 'The Eyes of Thomas',
        author: 'Yehuda Katz',
        body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }
];

var PHOTOS = [
    { id: 1, src: "/images/potd.png" },
    { id: 2, src: "/images/yohuda.jpg" },
    { id: 3, src: "/images/easter.jpg" }
];

// No-brainer auth: server will authenticate with
// username "ember" and password "casts", respond
// with a token, and forget the token when restarted.

var currentToken;
app.post('/auth.json', function(req, res) {

    var body = req.body,
        username = body.username,
        password = body.password;

    if (username == 'doccenter' && password == '123') {
// Generate and save the token (forgotten upon server restart).
        currentToken = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        res.send({
            success: true,
            token: currentToken
        });
    } else {
        res.send({
            success: false,
            message: 'Invalid username/password'
        });
    }
});

function validTokenProvided(req, res) {
    return true;
// Check POST, GET, and headers for supplied token.
    var userToken = req.body.token || req.param('token') || req.headers.token;

    if (!currentToken || userToken != currentToken) {
        res.send(401, { error: 'Invalid token. You provided: ' + userToken });
        return false;
    }

    return true;
}

app.get('/servidores', function(req, res) {
    if (validTokenProvided(req, res)) {
        res.send(SERVIDORES);
    }
});

app.get('/eventos', function(req, res) {
    if (validTokenProvided(req, res)) {
        res.send(EVENTOS);
    }
});

app.get('/articles.json', function(req, res) {
    if (validTokenProvided(req, res)) {
        res.send(ARTICLES);
    }
});

// Returns URL to pic of the day.
app.get('/photos.json', function(req, res) {
    if (validTokenProvided(req, res)) {
        res.send(PHOTOS);
    }
});



app.use(express.static(__dirname + '/public'));

app.listen(3000);
console.log('Listening on port 3000...');
